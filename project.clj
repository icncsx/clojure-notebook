(defproject my-project "0.1.0-SNAPSHOT"
  :description "Intro to Clojure notebook"
  :dependencies [[org.clojure/clojure "1.9.0"]]
  :main ^:skip-aot my-project.core
  :target-path "target/%s"
  :plugins [[lein-jupyter "0.1.16"]]
  :profiles {:uberjar {:aot :all}})